AWS S3 Plugin for CakePHP2
==========================

Functionality
-------------

This plugin is intended to be coupled with _[Cropify](http://codehub.alexseman.com/cropify)_ to automate the uploading of image assets on an Amazon S3 bucket.
Still, it can be used standalone and with any other type of files.


Installation
------------

1.	Clone the repo to the _APP/Plugin/S3_  directory (not necessarilly "S3", but be sure the name matches the plugin name you register at step 4).
2.	The AWS SDK is placed as a [git submodule](http://git-scm.com/book/en/Git-Tools-Submodules) so after cloning you need to init and update it separately. In your _APP/Plugin/S3_ directory run:

		~/htdocs/myCakeApp/app/Plugin/S3 $ git submodule init; git submodule update
		
3.	Copy _Vendor/aws-sdk-for-php/config-sample.inc.php_ to _Vendor/aws-sdk-for-php/config.inc.php_ and place your AWS credentials in the file.
4.	Let your application know about the plugin by appending to your _app/Config/bootstrap.php_ file: `CakePlugin::load('S3');`.
5.	If you're planning to use the plugin with _[Cropify](http://codehub.alexseman.com/cropify)_ then view the Usage section.
6.	If the plugin will be standalone then just register it's S3 component just like any other:

	* for the whole controller:
	
			public $components = array('S3.S3');
			
	* or only in the actions you need it:
	
			$s3Component = $this->Components->load('S3.S3');

		
Changelog
---------

*	__v1.0__ *17 July 2012*

	* dumped the S3Component;
	* the AWS SDK is a git submodule;
	* added README.md file.
	
Usage
-----

* If coupled with _[Cropify](http://codehub.alexseman.com/cropify)_ when adding the Cropify helper in your controller you need to also add the following keys in the config array like this:

		$this->helpers['Cropify.Cropify'] = array(
													// [other settings..]
													'S3' => true,
													'S3Bucket' => myS3Bucket
												);
From here Cropify will take over and load the component and use it.
	
													
* If standalone

	* upload:
	
			$url = $s3Component->upload($bucket, $absPath, $fileNameAndParentDir, 'mimeType');
			
	* deletion:
	
			$s3Component->delete($bucket, 'path');
