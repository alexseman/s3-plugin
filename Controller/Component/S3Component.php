<?php 
	require_once(APP . 'Plugin' . DS . 'S3' . DS . 'Vendor' . DS . 'aws-sdk-for-php' . DS . 'sdk.class.php');

	/**
	 * Amazon S3 Component
	 * 
	 * 
	 * 
	 * @author Alex Seman <mail@alexseman.com>
	 */
	class S3Component extends Component
	{
		private $s3;

		public function __construct()
		{
			$this->s3 = new AmazonS3();
		}


		/**
		 * Uploads a file from the server (usually from webroot) to
		 * Amazon S3 and returns the URL.
		 * 
		 * @param 	string 	$bucket name of the bucket where the files should be uploaded
		 * @param 	string 	$absPath the path of the file on the server
		 * @param 	string 	$path the portion of $absPath that consits only of the filename and it's parent directory
		 * @param 	string 	$mime content type of the file to be uploaded
		 * @return 	mixed 	boolean false if operation didn't succeed or string with URL of the file on the bucket otherwise
		 * @access public
		 */
		public function upload($bucket, $absPath, $path, $mime)
		{
			$exists = $this->s3->if_bucket_exists($bucket);
			while(!$exists)
			{
				sleep(1);
				$exists = $this->s3->if_bucket_exists($bucket);
			}

			$response = $this->s3->create_object(
													$bucket,
													$path,
													array 	(
																'fileUpload' => fopen($absPath, 'r'),
																'acl' => AmazonS3::ACL_PUBLIC,
																'contentType' => $mime,
																'storage' => AmazonS3::STORAGE_REDUCED,
																'headers' => array(
																						'Cache-Control' => 'max-age=604800, public',
																						'Last-Modified' => gmdate('D, d M Y H:i:s', time()) . ' GMT',
																						'Content-Length' => filesize($absPath)
																					),
															)
												);
			

			if($response->isOK())
			{
				unlink($absPath);
				return $this->s3->get_object_url($bucket, $path);
			}

			return false;
		}


		/**
		 * Deletes a file from the bucket.
		 * 
		 * @param 	string 		$bucket name of the bucket where the files should be uploaded
		 * @param 	string 		bucket-relative path of the file
		 * @return 	boolean 	with outcome of the operation
		 * @access public
		 */
		public function delete($bucket, $file)
		{
			$exists = $this->s3->if_bucket_exists($bucket);
			while(!$exists)
			{
				sleep(1);
				$exists = $this->s3->if_bucket_exists($bucket);
			}

			$response = $this->s3->delete_object($bucket, $file);
			return $response->isOK();
		}
	}
?>
